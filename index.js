#!/usr/bin/env node

const request = require('request');
const program = require("commander");
const cheerio = require("cheerio");

program.arguments('<url>')
	.option('')
	.action(function(url) {
		request("http://" + url, function (error, response, body) {
			console.log('error:', error);
			console.log('statusCode:', response && response.statusCode);
			const $ = cheerio.load(body)

			$('meta').each(function(i, e) {
				console.log($(this).attr());
			});
		});


		//		process.exit(0)
	})
	.parse(process.argv)

